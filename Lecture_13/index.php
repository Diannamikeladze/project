<?php
    require_once "config/configs.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blog</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <?php
        include_once "blocks/header.php";
    ?>
    <main>
        <?php
            include_once "blocks/nav.php";
            include_once "blocks/content.php";
        ?>
    </main>
    <?php
        include_once "blocks/footer.php";
    ?>
</body>
</html>