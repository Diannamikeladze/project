<?php
    include "questions.php";
    // echo "<pre>";
    // print_r($_POST);
    // echo "</pre>";
    $sum = 0;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Result</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="home">
        <form action="result.php" method="post">
            <h1>PHP Quiz - Result</h1>
            <div class="student-info">
                <h2>
                <?php
                    echo $_POST['st_name']." ".$_POST['st_lastname'];
                ?>
               </h2>
            </div>
            <table>
                <tr>
                    <th>Question</th>
                    <th>Answer</th>
                    <th>Max Point</th>
                    <th>Grade</th>
                </tr>
                <?php
                    foreach($questions as $key=>$question){
                    $sum += $_POST['grade'][$key];
                ?>
                <tr>
                    <td><?=$question['question']?></td>
                    <td><?=$_POST['answer'][$key]?></td>
                    <td><?=$question['max_point']?></td>
                    <td><?=$_POST['grade'][$key]?></td>
                </tr>
                <?php
                    }
                ?>
                 <tr>
                    <td colspan="3" style="text-align:right">Sum Of Points:</td>
                    <td><?=$sum?></td>
                </tr>
            </table>
            <button class="send">Result</button>
        </form>
    </div>
</body>
</html>