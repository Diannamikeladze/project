<?php
    $questions = [
        ['question'=>"What does PHP stand for?", 'max_point'=>5],
        ['question'=>"PHP server scripts are surrounded by delimiters, which?", 'max_point'=>4],
        ['question'=>"How do you write 'Hello World' in PHP", 'max_point'=>3],
        ['question'=>"All variables in PHP start with which symbol?", 'max_point'=>4],
        ['question'=>"What is the correct way to end a PHP statement?", 'max_point'=>4],
    ]
?>