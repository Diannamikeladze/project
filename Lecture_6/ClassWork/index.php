<?php
    session_start();
    $err_ms = [
        'saxeli'=>"",
        'gvari'=>"",
        'bool'=>FALSE
    ];

    $old_val = [
        'saxeli'=>"",
        'gvari'=>""
    ];

    if(isset($_POST['saxeli'])){
        if(strlen($_POST['saxeli'])<4){
            $err_ms['saxeli'] = "Is not valid";
            $err_ms['bool'] = TRUE;  
        }else{
            $old_val['saxeli'] = $_POST['saxeli'];
            $err_ms['bool'] = FALSE; 
        }
    }

    if(isset($_POST['send']) && $err_ms['saxeli']=="" && $err_ms['gvari']==""){
        $_SESSION['saxeli'] = $_POST['saxeli'];
        header("location:action.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lecture 6</title>
</head>
<body>
    <form method="post">
        <input type="text" name="saxeli" value="<?=$old_val['saxeli']?>"> - <?=$err_ms['saxeli']?>
        <br><br>
        <input type="text" name="gvari" value="<?=$old_val['gvari']?>">  - <?=$err_ms['gvari']?>
        <br><br>
        <button name="send">Send Info</button>
    </form>
</body>
</html>