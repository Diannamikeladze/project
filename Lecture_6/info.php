<?php 
    include "action.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <form action="" method="post" >
            <span>სახელი: </span><input type="text" name="name"><span><?php nameValidation() ?></span><br><br>
            <span>გვარი: </span><input type="text" name="surname"><span><?php surnameValidation() ?></span><br><br>

            <span>დაბადების თარიღი: </span>
            <select name="birthDay" id="">
                <?php 
                    year();
                ?>
            </select> 
            <select name="" id="">
                <?php 
                    month(); 
                ?>
            </select>
            <select name="" id="">
                <?php 
                    day();
                ?>
            </select>
            <br><br>


            <span>პირადი ნომერი: </span><input type="text" name="personalNumber"><span></span><br><br>
            <span>მისამართი: </span><input type="text" name="address"><br><br>
            <span>მობილური: </span><input type="text" name="mobileNumber"><br><br>
            <p>დამატებითი ინფორმაცია</p><textarea name="addtionalInfo" id="" cols="30" rows="10"></textarea>
            <br>
            <button type="submit">Send Info</button>
        </form>
    </div>
</body>
</html>