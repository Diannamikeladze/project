<?php
    if(isset($_POST['dir_name'])){
        include "includes/create_root_dir.php";
    }
    include "includes/dir_config.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
        if($root == ""){
            include "includes/create_folder_form.php";
        }
    ?>  
    <div class="content">
        <div class="left"></div>
        <div class="right">
            <div class="title">
                <h1><?=$root?></h1>
            </div>
            <div class="create_file">
                <h3>Create .tct File</h3>
                <form action="" method="post">
                    <input type="text" name="file_name">
                    <br><br>
                    <button>Create File</button>
                </form>
            </div>
            <div class="create_dir">
                <h3>Create Folder</h3>
                <form action="" method="post">
                    <input type="text" name="folder_name">
                    <br><br>
                    <button>Create Folder</button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>